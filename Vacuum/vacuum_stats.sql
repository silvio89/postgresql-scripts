WITH table_opts AS (
  SELECT
    pg_class.oid, relname, nspname, array_to_string(reloptions, '') AS relopts
  FROM
     pg_class INNER JOIN pg_namespace ns ON relnamespace = ns.oid
), vacuum_settings AS (
  SELECT
    oid, relname, nspname,
    CASE
      WHEN relopts LIKE '%autovacuum_vacuum_threshold%'
        THEN substring(relopts, '.*autovacuum_vacuum_threshold=([0-9.]+).*')::integer
        ELSE current_setting('autovacuum_vacuum_threshold')::integer
      END AS autovacuum_vacuum_threshold,
    CASE
      WHEN relopts LIKE '%autovacuum_vacuum_scale_factor%'
        THEN substring(relopts, '.*autovacuum_vacuum_scale_factor=([0-9.]+).*')::real
        ELSE current_setting('autovacuum_vacuum_scale_factor')::real
      END AS autovacuum_vacuum_scale_factor
  FROM
    table_opts
)
SELECT c.oid::regclass::text AS tbl, 
	   pg_size_pretty((st).table_len) as tbl_size, 
	   st.tuple_count as live, 
	   pg_size_pretty(tuple_len) as live_size,
	   st.dead_tuple_count as dead,
	   pg_size_pretty(st.dead_tuple_len) as dead_size,
	   st.dead_tuple_percent as "Dead %",
	   round(float8(autovacuum_vacuum_scale_factor * 100)::numeric, 10) as "Scale Factor %",
     autovacuum_vacuum_threshold as n_lines_to_trigger_vacuum
FROM   pg_class c
INNER JOIN vacuum_settings ON c.oid = vacuum_settings.oid   
JOIN   pg_namespace n ON n.oid = c.relnamespace
      ,pgstattuple(c.oid::regclass::text) st
WHERE  c.relkind = 'r'            -- only tables
AND    n.nspname NOT LIKE 'pg_%'  -- exclude system schemas
ORDER  BY 7 DESC;